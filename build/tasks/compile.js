var msbuild = require('gulp-msbuild');
var gulp = require('gulp');

gulp.task('compile', function() {
    return gulp
        .src('**/Titan.sln')
        //.pipe(msbuild());
		.pipe(msbuild({
            //targets: ['Clean', 'Release'],
            toolsVersion: 14.0,
            errorOnFail: true,
            stdout: true
        }));
});

gulp.task('compile-mono', function() {
    return gulp
        .src('**/Titan.Mono.sln')
        //.pipe(msbuild());
		.pipe(msbuild({
            //targets: ['Clean', 'Release'],
            //toolsVersion: 14.0,
            errorOnFail: true,
            stdout: true
        }));
});