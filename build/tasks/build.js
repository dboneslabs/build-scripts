var gulp = require('gulp');
var runSequence = require('run-sequence');


gulp.task('build-all', function(done) {

    runSequence(
        ['assemblyInfo',
            'restore-dependencies',
            'nuspec-update-meta'],
        'nuspec-sync-dependency-version',
        'compile',
        //'test',
        'package',
        //'push',
        done);

});