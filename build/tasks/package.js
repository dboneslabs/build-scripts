var gulp = require('gulp');
var request = require('request');
var mkdirp = require('mkdirp');
var Nuget = require('nuget-runner');
var glob = require("glob");
var Promise = require('es6-promise').Promise;
var path = require('path');
var config = require('../config');
var eyes = require('eyes');

var outputDir = config.outputDir;
var toolsDir = config.toolsDir;
var nugetDir = toolsDir + '/nuget';
var runnerFileName = nugetDir + '/nuget.exe';


var defaultSettings = {
    nugetPath: runnerFileName,
    verbosity: 'detailed'
};

if (config.command.package.configFile != null) {
    // The NuGet configuation file. If not specified, file
    // %AppData%\NuGet\NuGet.config is used as configuration file.
    //configFile: 'path/to/nuget.config'
    defaultSettings.configFile = config.command.package.configFile;
}

var nuget = Nuget(defaultSettings);


gulp.task('package', ['get-nuget'], function () {

    mkdirp.sync(outputDir);

    procFiles('./**/*.nuspec', function (file) {

        var nextPromise = new Promise(function (resolve, reject) {

            var arg = {
                spec: path.resolve(file),
                outputDirectory: outputDir,
                basePath: path.resolve('./')
            };

            if (config.buildVersion) {
                arg.version = config.buildVersion;
            }

            nuget.pack(arg)
                .then(function () {
                    console.log('done');
                    resolve();
                })
                .fail(function (error) {
                    console.log(error.message);
                    reject(error.message);
                });

        });
        return nextPromise;
    })
        .then(function () { done(); });

});

gulp.task('push', ['get-nuget'], function (done) {

    mkdirp.sync(outputDir);
    procFiles(outputDir + '/**/*.nupkg', function (file) {
        var nextPromise = new Promise(function (resolve, reject) {

            var settings = config.command.package;

            var arg = {
                // Specifies the server URL. If not specified, nuget.org is used unless  
                // `DefaultPushSource` config value is set in the NuGet config file. Starting  
                // with NuGet 2.5, if NuGet.exe identifies a UNC/folder source, it will  
                // perform the file copy to the source. 
                source: settings.pushTo,
                apiKey: settings.apiKey
            };

            nuget.push(file, arg)
                .then(function () {
                    console.log('done');
                    resolve();
                })
                .fail(function (error) {
                    console.log(error.message);
                    reject(error.message);
                });

        });
        return nextPromise;
    })
        .then(function () { done(); });

});

gulp.task('restore-dependencies', ['get-nuget'], function (done) {

    mkdirp.sync(outputDir);

    procFiles('./**/*.sln', function (file) {

        var nextPromise = new Promise(function (resolve, reject) {

            var arg = {

                // Specify the solution path or path to a packages.config file.
                packages: path.resolve(file),

                // A list of packages sources to use for the install.
                // Can either be a path, url or config value.
                //source: ['http://mynugetserver.org', 'path/to/source', 'ConfigValue'],

                noCache: true,
                requireConsent: false,
                disableParallelProcessing: false,
                verbosity: 'detailed'
                //configFile: 'path/to/nuget.config'

            };

            nuget.restore(arg)
                .then(function () {
                    console.log('done');
                    resolve();
                })
                .fail(function (error) {
                    console.log(error.message);
                    reject(error.message);
                });

        });
        return nextPromise;
    })
        .then(function () { done(); });
});


/**
 * process a number of files on afer the other
 * @param actionOnItem delegate to call on each file (takes a file name, returns a promise)
 * @param filter how to find the correct files
 */
function procFiles(filter, actionOnItem) {
    var promise = new Promise(function (resolve) { resolve(); });

    glob(filter, function (er, files) {
        eyes.inspect(files);
        files.forEach(function (file) {
            promise = promise.then(function () {
                return actionOnItem(file);
            });
            promise['catch'](function (err) {
                console.log(err);
            });

        });

    });

    return promise;
};