var args = require('yargs').argv;
var path = require('path');

//globals
var config = {

    outputDir : path.resolve('./output'),
    toolsDir : path.resolve('./tools'),
    buildVersion : args.buildNumber ? '0.2.' + args.buildNumber  : '0.0.0',
    company: 'dbones.co.uk'

};

//per command config
config.command = {
    assembly: {
        copyright: 'Copyright '+ config.company +' 2012-' + new Date().getFullYear()
    },
    test:{
        dllName: '*.Test.dll'
    },
    package: {
        //dependencyNameOverride: '//x:dependency[starts-with(@x:id, \'Boxes.\')]/@x:version'
        //configFile: null
        source: ['https://api.nuget.org/v3/index.json'], //source: ['http://mynugetserver.org', 'path/to/source', 'ConfigValue']
        //push details
        apiKey: '',
        pushTo: '',
        //set metadata in the nuspec
        metadata:{
            authors:'dbones labs',
            owners: config.company,
            copyright: 'Copyright '+ config.company +' 2012-' + new Date().getFullYear(),
            licenseUrl: 'http://choosealicense.com/licenses/no-license/'
		}
    }
};

module.exports = config;